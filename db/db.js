{
	"_id": ObjectId("5e85fce4550107cd62076db2"),
	"titulo": "O pequeno príncipe - Edição de bolso",
	"ISBN": "9788561706210",
	"autor": "Harper Collins",
	"editora": "Antoine de Saint-Exupéry",
	"ano": "2011",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85fd2d550107cd62076dc5"),
	"titulo": "O historiador",
	"ISBN": "9788573029932",
	"autor": "Ponto",
	"editora": "Elizabeth Kostova",
	"ano": "2009",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85fd9b550107cd62076dda"),
	"titulo": "Prazeres Malditos",
	"ISBN": "9788532522498",
	"autor": "Rocco",
	"editora": "Laurell K. Hamilton",
	"ano": "2013",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85fe5b550107cd62076def"),
	"titulo": "Operação Valquíria",
	"ISBN": "9788501082978",
	"autor": "Record",
	"editora": "Phillip Freiherr",
	"ano": "2011",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85fe9a550107cd62076df7"),
	"titulo": "O CAO DOS BASKERVILLE",
	"ISBN": "9788599187395",
	"autor": "DIGERATI",
	"editora": "Arthur Conan Dayle",
	"ano": "2002",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85fed0550107cd62076dff"),
	"titulo": "Fallen (Vol. 1)",
	"ISBN": "9788501089625",
	"autor": "Galera",
	"editora": "Lauren Kate",
	"ano": "2006",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85fefe550107cd62076e06"),
	"titulo": "Gomorra",
	"ISBN": "9788528613681",
	"autor": "Bertrand",
	"editora": "Roberto Saviano",
	"ano": "2011",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85ff3e550107cd62076e10"),
	"titulo": "A sombra do vento",
	"ISBN": "9788560280094",
	"autor": "Suma",
	"editora": "Carlos Ruiz Zafón",
	"ano": "2012",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85ff7a550107cd62076e15"),
	"titulo": "O Último Segredo do Templo",
	"ISBN": "978852863742",
	"autor": "Bertrand",
	"editora": "Paul Sussman",
	"ano": "2014",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85ffb7550107cd62076e20"),
	"titulo": "O Segredo do Anel. O Legado de Maria Madalena",
	"ISBN": "9788532520968",
	"autor": "Rocco",
	"editora": "Kathleen McGowan",
	"ano": "1998",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
{
	"_id": ObjectId("5e85ffdb550107cd62076e23"),
	"titulo": "Labirinto",
	"ISBN": "9788573027686",
	"autor": "Suma",
	"editora": "Kate Mosse",
	"ano": "2015",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
