#### MUITO IMPORTANTE !!!
-  Para rodar o backend - "yarn dev"
-  Para rodar o fronten - "yarn start"

#### Estou usando as seguintes tecnologias - Leia a Documentação

-   [React](https://pt-br.reactjs.org/docs/getting-started.html) Obs: ler documentação
-   [Redux](https://redux.js.org/) Obs: ler documentação
-   [Redux-Saga](https://redux-saga.js.org/) Obs: ler documentação
-   [React Router](https://reacttraining.com/react-router/web/guides/quick-start) Obs: ler documentação
-   [ES6](http://es6-features.org/) Obs: ler documentação
-   [styled-components](https://www.styled-components.com/) Obs: ler documentação
-   [Bootstrap](https://getbootstrap.com/) Obs: ler documentação
-   [NodeJS](https://nodejs.org/en/docs/) Obs: ler documentação
-   [Express](https://expressjs.com/pt-br/) Obs: ler documentação
-   [MongoDB](https://www.mongodb.com/) Obs: ler documentação

## BACKEND - NODEJS MONGODB

- Instale o [MongoDB](https://www.mongodb.com/)
-   Instale o [Robo 3T (formerly Robomongo)](https://robomongo.org/download) para facilitar a visualização do banco de dados
-   crie um database chamado [searchBooks]()
-   crie collection chamadas [books]()
-   Executar no terminal "cd ./backend"
-   Executar no terminal "yarn install"
-   Executar no terminal "yarn dev"
-   copie e cole no shell do robo para iserer os registro faça com cada um que esta na pasta db/db.js
```
db.books.insert( {
	"_id": ObjectId("5e85fce4550107cd62076db2"),
	"titulo": "O pequeno príncipe - Edição de bolso",
	"ISBN": "9788561706210",
	"autor": "Harper Collins",
	"editora": "Antoine de Saint-Exupéry",
	"ano": "2011",
	"idioma": "Português",
	"peso": "20",
	"comprimento": "20",
	"largura": "20",
	"altura": "20"
}
)
```

## FRONTEND - REACT

-   Executar no terminal "cd ./front-end"
-   Executar no terminal "yarn install"
-   Executar no terminal "yarn start"
