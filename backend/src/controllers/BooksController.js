const mongoose = require('mongoose');
const Book = mongoose.model('Books');

module.exports = {
  async index(req, res) {
    const books = await Book.find();

    return res.json(books);
  },

  async detailsBook(req, res) {
    const search = req.body.id;

    const books = await Book.find(
      {
        $or: [
          { "_id": { "$in": [search] } },
        ]
      }
    )

    return res.json(books)
  },

  async search(req, res) {
    const search = req.body.search;

    if (search == '') {
      const books = await Book.find();

      return res.json(books);
    } else {
      const books = await Book.find(
        {
          $or: [
            { "titulo": { "$in": [search] } },
            { "autor": { "$in": [search] } },
            { "ISBN": { "$in": [search] } }
          ]
        }
      )

      return res.json(books)
    }
  },

  async searchYear(req, res) {
    const yearStart = req.body.start;
    const yearEnd = req.body.end;

    if (yearStart == '' && yearEnd == '') {
      const books = await Book.find();

      return res.json(books);
    } else {
      const books = await Book.find(
        {
          $or: [
            { "ano": { "$in": [yearStart, yearEnd] } },
          ]
        }
      )

      return res.json(books)
    }
  }
}
