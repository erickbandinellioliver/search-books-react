const mongoose = require('mongoose');

const BooksSchema = new mongoose.Schema({
  title: {
    type: String,
    trim: true,
    index: true,
    required: true,
  },
  ISBN: {
    type: String,
    trim: true,
    index: true,
    required: true
  },
  autor: {
    type: String,
    trim: true,
    index: true,
    required: true
  },
  editora: {
    type: String,
    trim: true,
    required: true
  },
  ano: {
    type: String,
    trim: true,
    index: true,
    required: true
  },
  idioma: {
    type: String,
    trim: true,
    required: true
  },
  peso: {
    type: String,
    trim: true,
    required: true
  },
  comprimento: {
    type: String,
    trim: true,
    required: true
  },
  largura: {
    type: String,
    trim: true,
    required: true
  },
  altura: {
    type: String,
    trim: true,
    required: true
  },
  createAt: {
    type: Date,
    default: Date.now
  },
});

mongoose.model('Books', BooksSchema);
