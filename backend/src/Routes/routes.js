const express = require('express');
const routes = express.Router();

const BooksController = require('../controllers/BooksController');

routes.get('/list-books', BooksController.index);
routes.post('/search-books', BooksController.search);
routes.post('/search-year-books', BooksController.searchYear);
routes.post('/details-book', BooksController.detailsBook);

module.exports = routes;
