import React from 'react';
import Header from '../../components/Header';
import Filter from '../../components/Filter';
import DataTable from '../../components/DataTable';

export default function Home() {
  return (
    <React.Fragment>
      <Header />
      <Filter />
      <DataTable />
    </React.Fragment>
  );
};
