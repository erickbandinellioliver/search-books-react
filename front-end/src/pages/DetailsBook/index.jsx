import React from 'react';
import Header from '../../components/Header';
import DetailsBook from '../../components/DetailsBook';

export default function Details() {
  return (
    <React.Fragment>
      <Header />
      <DetailsBook />
    </React.Fragment>
  );
};
