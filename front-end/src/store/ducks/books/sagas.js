import { call, put } from 'redux-saga/effects';
import api from '../../../services/api';

import { loadSucccesBooks, loadFailureBooks } from './actions';

export function* loadBooks() {
	try {
		const response = yield call(api.get, '/list-books');
		yield put(loadSucccesBooks(response.data));
	} catch (err) {
		yield put(loadFailureBooks());
	}
}

export function* searchBooks(action) {
	const search = action.payload.search
	try {
		const response = yield call(
			api.post,
			'/search-books',
			{
				search
			});
		yield put(loadSucccesBooks(response.data));
	} catch (err) {
		yield put(loadFailureBooks());
	}
}

export function* detailsBooks(action) {
	const id = action.payload.id
	try {
		const response = yield call(
			api.post,
			'/details-book',
			{
				id
			});
		yield put(loadSucccesBooks(response.data));
	} catch (err) {
		yield put(loadFailureBooks());
	}
}


export function* searchYearBooks(action) {
	const yearStart = action.payload.yearStart
	const yearEnd = action.payload.yearEnd
	try {
		const response = yield call(
			api.post,
			'/search-year-books',
			{
				start: yearStart,
				end: yearEnd
			});
		yield put(loadSucccesBooks(response.data));
	} catch (err) {
		yield put(loadFailureBooks());
	}
}
