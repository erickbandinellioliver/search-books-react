export const loadRequestBooks = () => {
	return {
		type: "LOAD_REQUEST_BOOKS"
	}
}

export const loadRequestSearchBooks = (search) => {
	return {
		type: "LOAD_REQUEST_SEARCH_BOOKS",
		payload: { search }
	}
}

export const loadRequestDetailsBooks = (id) => {
	return {
		type: "LOAD_REQUEST_DETAILS_BOOKS",
		payload: { id }
	}
}

export const loadRequestSearchYearBooks = (yearStart, yearEnd) => {
	return {
		type: "LOAD_REQUEST_SEARCH_YEAR_BOOKS",
		payload: { yearStart, yearEnd }
	}
}

export const loadSucccesBooks = (data) => {
	return {
		type: "LOAD_SUCCCES_BOOKS",
		payload: data
	}
}

export const loadFailureBooks = () => {
	return {
		type: "LOAD_FAILURE_BOOKS"
	}
}
