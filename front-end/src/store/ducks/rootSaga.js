import { all, takeLatest } from 'redux-saga/effects';
import { loadBooks, searchBooks, searchYearBooks, detailsBooks } from './books/sagas';

export default function* rootSaga() {
	return yield all([
		takeLatest('LOAD_REQUEST_BOOKS', loadBooks),
		takeLatest('LOAD_REQUEST_SEARCH_BOOKS', searchBooks),
		takeLatest('LOAD_REQUEST_SEARCH_YEAR_BOOKS', searchYearBooks),
		takeLatest('LOAD_REQUEST_DETAILS_BOOKS', detailsBooks),
	])
}
