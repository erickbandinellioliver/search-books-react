import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { loadRequestBooks } from '../../store/ducks/books/actions';
import DataTableStyle from './style';

import Pagination from "../Pagination";

const DataTable = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [booksPerPage] = useState(5);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadRequestBooks());
  }, [dispatch]);

  const booksState = useSelector(
    (state) => state.Books
  );

  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  const indexOfLastBooks = currentPage * booksPerPage;
  const indexOfFirstBooks = indexOfLastBooks - booksPerPage;
  //Order Alphabetic
  const newListBooks = booksState.data;
  const currentBooks = newListBooks.slice(indexOfFirstBooks, indexOfLastBooks);
  const qtnBooks = newListBooks.length;

  return (
    <DataTableStyle>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12 col-md-12 col-lg-12">
            <div className="table-responsive">
              <table className="table">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">Título</th>
                    <th scope="col">Autor</th>
                    <th scope="col">Editora</th>
                    <th scope="col">Ano</th>
                    <th scope="col">Ações</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    currentBooks.map((item, key) => {
                      return (
                        <tr key={key}>
                          <td>
                            {item.titulo}<br></br>
                            <span>({item.ISBN})</span>
                          </td>

                          <td>
                            {item.autor}
                          </td>

                          <td>
                            {item.editora}
                          </td>

                          <td>
                            {item.ano}
                          </td>

                          <td>
                            <a href={`details/${item._id}`} className="btn btn-primary">Detalhes</a>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            </div>
          </div>

          <div className="col-sm-12 col-md-12 col-lg-12">
            <Pagination
              currentPage={currentPage}
              booksPerPage={booksPerPage}
              totalBooks={qtnBooks}
              paginate={paginate} />
          </div>
        </div>
      </div>
    </DataTableStyle>
  )
}

export default DataTable;
