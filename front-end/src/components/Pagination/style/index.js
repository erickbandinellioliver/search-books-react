
import styled from 'styled-components';

const PaginationStyle = styled.div`
    .pagination {
        justify-content: center;

        .page-item {
            cursor: pointer;

            &.active {
                .page-link {
                    background-color: #f5d67f;
                    border-color: #f5d67f;
                    color: #fff;
                }
            }

            .page-link {
                color: #f5d67f;
            }
        }
    }
`;

export default PaginationStyle;
