import React, { useEffect } from 'react';
import { useParams } from "react-router";
import { useSelector, useDispatch } from 'react-redux';
import { loadRequestDetailsBooks } from '../../store/ducks/books/actions';
import DetailsBookStyle from './style';

const DetailsBook = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadRequestDetailsBooks(id));
  }, [dispatch]);

  const booksState = useSelector(
    (state) => state.Books
  );

  const newListBooks = booksState.data;
  return (
    <DetailsBookStyle>
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-12 col-md-12 col-lg-12">
            <a href="/" className="btn btn-primary">Retornar ao início</a>
          </div>
          <div className="col-sm-12 col-md-12 col-lg-12">
            <div className="table-responsive">
              <table className="table">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">Título</th>
                    <th scope="col">Autor</th>
                    <th scope="col">Editora</th>
                    <th scope="col">Ano</th>
                    <th scope="col">Idioma</th>
                    <th scope="col">Peso</th>
                    <th scope="col">Comprimento</th>
                    <th scope="col">Largura</th>
                    <th scope="col">Altura</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    newListBooks.map((item, key) => {
                      return (
                        <tr key={key}>
                          <td>
                            {item.titulo}<br></br>
                            <span>({item.ISBN})</span>
                          </td>

                          <td>
                            {item.autor}
                          </td>

                          <td>
                            {item.editora}
                          </td>

                          <td>
                            {item.ano}
                          </td>

                          <td>
                            {item.idioma}
                          </td>

                          <td>
                            {item.peso} g
                          </td>

                          <td>
                            {item.comprimento} cm
                          </td>

                          <td>
                            {item.largura} cm
                          </td>

                          <td>
                            {item.altura} cm
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </DetailsBookStyle >
  )
}

export default DetailsBook;
