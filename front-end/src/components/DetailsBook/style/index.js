import styled from 'styled-components';

const DetailsBookStyle = styled.section`
    display: block;
    margin: 48px auto;
    padding: 20px 21px;
    width: 96%;
    background-color: #fff;

    h1 {
        color: #333;
        font-family: 'Coustard',serif;
        font-size: 25px;
        padding: 8px 0;
    }

    .btn-primary {
      color: #fff;
      background-color: #7a00ff;
      border-color: #7a00ff;
      width: 150px;
      margin: 0 0 13px 0;
      font-size: 15px;

      &:hover {
        transition: all 0.2s ease-in-out;
        background-color: #490098;
        border-color: #490098;
      }
    }

    .table-responsive{
        overflow-x: initial;

        @media screen and (max-width: 700px){
            overflow-x: scroll;
        }

        .table {
            .thead-dark {
                border: 1px solid #f0efef;

                th {
                  color: #191313;
                  border-color: #ffffff;
                  background-color: #f5d67f;
                  font-family: 'Open Sans', sans-serif;
                  font-size: 15px;
                }
            }

            tbody {
                border: 1px solid #f0efef;

                tr {
                    color: #333;
                    font-family: 'Open Sans', sans-serif;
                    font-size: 12px;
                    letter-spacing: 1px;
                    opacity: 1;

                    td {
                        &:nth-child(1) {
                            width: 300px;
                        }
                    }
                }
            }
        }
    }
`;

export default DetailsBookStyle;
