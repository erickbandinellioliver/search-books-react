import React, { useState } from "react";
import { useDispatch } from 'react-redux';
import { loadRequestSearchBooks } from '../../store/ducks/books/actions';
import HeaderStyle from './style';

const Header = () => {
	const [search, setSearch] = useState('');
	const dispatch = useDispatch();

	function handleSearch() {
		dispatch(loadRequestSearchBooks(search));
	}

	return (
		<HeaderStyle>
			<div className="container-fluid">
				<div className="row">
					<div className="col-sm-12 col-md-2 col-lg-2">
						<h1 className="logo">Supero</h1>
					</div>
					<div className="col-sm-12 col-md-10 col-lg-10">
						<div className="form">
							<input
								type="search"
								placeholder="Busque livros pelo titulo, autor ou ISBN"
								onChange={(e) => setSearch(e.target.value)}
								required />
							<button type="submit" className="btn btn-primary" onClick={(e) => handleSearch()}>Buscar</button>
						</div>
					</div>
				</div>
			</div>
		</HeaderStyle>
	);
};

export default Header;
