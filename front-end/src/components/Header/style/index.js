import styled from 'styled-components';

const HeaderStyle = styled.header`
		border-bottom: 1px solid #ccc;
		
		h1 {
			color: #333;
			letter-spacing: 0.3rem;
			text-transform: uppercase;
			font-size: 30px;
			text-align: center;
			font-weight: bold;
			margin: 29px 0 29px 0;
		}

		input {
			height: 30px;
			width: 70%;
			padding: 22px 0 22px 18px;
			border: 1px solid #ccc;
			margin: 24px 0;

			&:focus {
				border: 1px solid #7a00ff;
    		outline: none;
			}
		}

		.btn-primary {
			color: #fff;
			background-color: #7a00ff;
			border-color: #7a00ff;
			width: 21%;
			margin: -6px 0 0 3%;
			height: 45px;
			letter-spacing: 2px;
			font-weight: bold;

			&:hover {
				transition: all 0.2s ease-in-out;
				background-color: #490098;
				border-color: #490098;
			}
		}
`;

export default HeaderStyle;
