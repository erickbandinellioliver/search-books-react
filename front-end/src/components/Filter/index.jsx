import React, { useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { loadRequestSearchYearBooks } from '../../store/ducks/books/actions';
import FilterStyle from './style';

const Filter = () => {
	const dispatch = useDispatch();

	const [yearStart, setYearStart] = useState('');
	const [yearEnd, setYearEnd] = useState('');

	const booksState = useSelector(
		(state) => state.Books
	);

	function handleSearchYear() {
		dispatch(loadRequestSearchYearBooks(yearStart, yearEnd));
	}

	return (
		<FilterStyle>
			<div className="container-fluid">
				<div className="row">
					<div className="col-sm-12 col-md-9 col-lg-9">

						<div className="form-filter">
							<label htmlFor="inicio">Filtrar ano de publicação</label>
							<input type="text" id="inicio" onChange={(e) => setYearStart(e.target.value)} placeholder="Ano" required />

							<label htmlFor="fim">até</label>
							<input type="text" id="fim" onChange={(e) => setYearEnd(e.target.value)} placeholder="Ano" required />
							<button type="submit" className="btn btn-primary" onClick={(e) => handleSearchYear()}>Filtar</button>
						</div>
					</div>
					<div className="col-sm-12 col-md-3 col-lg-3">
						<span>{booksState.data.length} resultados encontrados</span>
					</div>
				</div>
			</div>
		</FilterStyle>
	);
};

export default Filter;
