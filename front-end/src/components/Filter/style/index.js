import styled from 'styled-components';

const FilterStyle = styled.section`
	border-bottom: 1px solid #ccc;

	label {
		padding: 0 13px;
	}

	span {
		padding: 31px 0 0 0;
    display: block;
	}

	input {
		height: 30px;
		width: 100px;
		padding: 22px 10px 22px 10px;
		border: 1px solid #ccc;
		margin: 24px 0;
		background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  90% 50% no-repeat ;

		&:focus {
			border: 1px solid #7a00ff;
			outline: none;
		}
	}

	.btn-primary {
			color: #fff;
			background-color: #7a00ff;
			border-color: #7a00ff;
			width: 100px;
			margin: -6px 0 0 3%;
			height: 45px;
			letter-spacing: 2px;
			font-weight: bold;

			&:hover {
				transition: all 0.2s ease-in-out;
				background-color: #490098;
				border-color: #490098;
			}
		}
`;

export default FilterStyle;
